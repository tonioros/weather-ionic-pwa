import {OpenwthBaseModel} from './openwth-base.model';
import {OpenwthCityModel} from './openwth-city.model';

export class OpenwthDailyForecastModel {
    cod: string;
    message: number;
    cnt: number;
    list: OpenwthBaseModel[];
    city: OpenwthCityModel;
}
