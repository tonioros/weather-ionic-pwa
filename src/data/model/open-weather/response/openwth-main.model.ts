export class OpenwthMainModel {
    temp: number;
    pressure: number;
    humidity: number;
    // tslint:disable-next-line:variable-name
    temp_min: number;
    // tslint:disable-next-line:variable-name
    temp_max: number;
}
