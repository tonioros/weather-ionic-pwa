import {OpenwthCordModel} from './openwth-cord.model';
import {OpenwthDayWeatherModel} from './openwth-day-weather.model';
import {OpenwthMainModel} from './openwth-main.model';
import {OpenwthWindModel} from './openwth-wind.model';
import {OpenwthSysModel} from './openwth-sys.model';

export class OpenwthBaseModel {
    coord: OpenwthCordModel;
    weather: OpenwthDayWeatherModel[];
    base: string;
    main: OpenwthMainModel;
    wind: OpenwthWindModel;
    dt: number;
    // tslint:disable-next-line:variable-name
    dt_txt: string;
    sys: OpenwthSysModel;
    timezone: string;
    id: number;
    name: string;
    cod: number;
    hour?: string;
}
