import {OpenwthCordModel} from './openwth-cord.model';

export class OpenwthCityModel {
    id: number;
    name: string;
    coord: OpenwthCordModel;
    country: string;
    timezone: number;
    sunrise: number;
    sunset: number;
}
