export class OpenwthDayWeatherModel {
    id: number;
    main: string;
    description: string;
    icon: string;
}
