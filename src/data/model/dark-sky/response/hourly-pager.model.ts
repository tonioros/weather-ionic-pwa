/**
 * Represent a list of weather details hour by hour
 */
import {HourlyWeatherModel} from './hourly-weather.model';

export class HourlyPagerModel {
    summary: string;
    icon: string;
    data: HourlyWeatherModel[];
}
