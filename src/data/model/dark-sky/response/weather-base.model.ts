import {HourlyWeatherModel} from './hourly-weather.model';
import {HourlyPagerModel} from './hourly-pager.model';

/**
 * Represent a base object of Weather state
 */
export class WeatherBaseModel {
    latitude: number;
    longitude: number;
    timezone: string;
    currently: HourlyWeatherModel;
    hourly: HourlyPagerModel;
    offset: number;
}
