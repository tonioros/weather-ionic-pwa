import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {WeatherBaseModel} from '../model/dark-sky/response/weather-base.model';
import {BaseCloudProvider} from './base.cloud-provider';
import {HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class DartSkyCloudProvider extends BaseCloudProvider {
    private API_BASE_URL = 'https://api.darksky.net/forecast/';
    private HEROKU_BASE_URL = 'https://cors-anywhere.herokuapp.com/';

    constructor() {
        super();
        this.API_TOKEN = environment.dark_sky_apitoken;
    }

    public getForecastByHourly(lat: number, lon: number) {
        const URL = `${this.HEROKU_BASE_URL}${this.API_BASE_URL}${this.API_TOKEN}/${lat},${lon}`;
        const headers = new HttpHeaders()
            .set('Content-Type', 'application/json; charset=utf-8')
            .set('X-Requested-With', 'XMLHttpRequest')
        return this.httpClient.get<WeatherBaseModel>(URL, {headers});
    }
}
