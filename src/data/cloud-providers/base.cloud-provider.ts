import {HttpClient, HttpHeaders} from '@angular/common/http';
import {injector} from '../data.module';

export abstract class BaseCloudProvider {
    protected API_TOKEN = '';
    protected httpClient: HttpClient;

    constructor() {
        this.httpClient = injector.get(HttpClient);
    }
}
