import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {OpenwthBaseModel} from '../model/open-weather/response/openwth-base.model';
import {OpenwthDailyForecastModel} from '../model/open-weather/response/openwth-daily-forecast.model';
import {BaseCloudProvider} from './base.cloud-provider';
import {HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class OpenWeatherCloudProvider extends BaseCloudProvider {
    private API_BASE_URL = 'http://api.openweathermap.org/data/2.5/';
    private HEROKU_BASE_URL = 'https://cors-anywhere.herokuapp.com/';

    constructor() {
        super();
        this.API_TOKEN = environment.open_weather_apitoken;
    }

    public getDailyWeather(lat: number, lon: number): Observable<OpenwthBaseModel> {
        const URL = `${this.HEROKU_BASE_URL}${this.API_BASE_URL}weather?appid=${this.API_TOKEN}&` +
            `lang=es&lat=${lat}&lon=${lon}&units=metric`;
        const headers = new HttpHeaders()
            .set('Content-Type', 'application/json; charset=utf-8')
            .set('X-Requested-With', 'XMLHttpRequest');
        return this.httpClient.get<OpenwthBaseModel>(URL, {headers});
    }

    public get5DaysForecast(lat: number, lon: number): Observable<OpenwthDailyForecastModel> {
        const URL = `${this.HEROKU_BASE_URL}${this.API_BASE_URL}forecast?appid=${this.API_TOKEN}&` +
            `lang=es&lat=${lat}&lon=${lon}&units=metric`;
        const headers = new HttpHeaders()
            .set('Content-Type', 'application/json; charset=utf-8')
            .set('X-Requested-With', 'XMLHttpRequest');
        return this.httpClient.get<OpenwthDailyForecastModel>(URL, {headers});
    }
}
