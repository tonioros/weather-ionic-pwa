import {Injector, NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';

export let injector: Injector;

@NgModule({
    imports: [
        HttpClientModule,
        IonicStorageModule.forRoot(),
    ],
})
export class DataModule {
    constructor(private inj: Injector) {
        injector = this.inj;
    }
}
