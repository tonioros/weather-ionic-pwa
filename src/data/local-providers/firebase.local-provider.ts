import {Injectable} from '@angular/core';
import {BaseLocalProvider} from './base.local-provider';
import {from, Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class FirebaseLocalProvider extends BaseLocalProvider {
    constructor() {
        super();
    }

    saveLocalToken(token: string) {
        this.storage.set('fcm-token', token);
    }

    getLocalToken(): Observable<string> {
        return from(this.storage.get('fcm-token'));
    }
}
