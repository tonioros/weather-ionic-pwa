import {Injectable} from '@angular/core';
import {OpenwthBaseModel} from '../model/open-weather/response/openwth-base.model';
import {WeatherBaseModel} from '../model/dark-sky/response/weather-base.model';
import {from, Observable} from 'rxjs';
import {BaseLocalProvider} from './base.local-provider';
import {OpenwthDailyForecastModel} from '../model/open-weather/response/openwth-daily-forecast.model';

@Injectable({
    providedIn: 'root'
})
export class WeatherLocalProvider extends BaseLocalProvider {

    public setWeatherNow(weather: OpenwthBaseModel): void {
        this.storage.set('weather_now', weather);
    }

    public setForecastByHourly(weather: WeatherBaseModel): void {
        this.storage.set('forecast_hourly', weather);
    }

    public getWeatherNow(): Observable<OpenwthBaseModel> {
        return from(this.storage.get('weather_now'));
    }

    public getForecastByHourly(): Observable<WeatherBaseModel> {
        return from(this.storage.get('forecast_hourly'));
    }

    public set5DaysForecast(weather: OpenwthDailyForecastModel): void {
        this.storage.set('five_days', weather);
    }

    public get5DaysForecast(): Observable<OpenwthDailyForecastModel> {
        return from(this.storage.get('five_days'));
    }

}
