import {Storage} from '@ionic/storage';
import {injector} from '../data.module';

export abstract class BaseLocalProvider {
    protected storage: Storage;

    constructor() {
        this.storage = injector.get(Storage);
        this.init();
    }

    private init(): void {
        this.storage.ready().then(value => {
            console.log('Storage is Ready');
        });
    }
}
