import {WeatherBaseModel} from '../model/dark-sky/response/weather-base.model';
import {Observable} from 'rxjs';
import {DartSkyCloudProvider} from '../cloud-providers/dart-sky.cloud-provider';
import {OpenWeatherCloudProvider} from '../cloud-providers/open-weather.cloud-provider';
import {OpenwthBaseModel} from '../model/open-weather/response/openwth-base.model';
import {OpenwthDailyForecastModel} from '../model/open-weather/response/openwth-daily-forecast.model';
import {Injectable} from '@angular/core';
import {NetworkStatus, Plugins} from '@capacitor/core';
import {WeatherLocalProvider} from '../local-providers/weather.local-provider';
import {catchError, map} from 'rxjs/operators';
import {EventBusService} from '../event-bus/event-bus.service';

@Injectable({
    providedIn: 'root'
})
export class WeatherDataProvider {
    private networkStatus: NetworkStatus;

    constructor(private dartSkyCloudProvider: DartSkyCloudProvider,
                private openWeatherCloudProvider: OpenWeatherCloudProvider,
                private offlineData: WeatherLocalProvider) {
        this.init();
    }

    getForecastByHourly(lat: number, lon: number): Observable<WeatherBaseModel> {
        if (this.isNetworkConnected()) {
            return this.dartSkyCloudProvider.getForecastByHourly(lat, lon)
                .pipe(map(response => {
                    const toGuardResponse = response;
                    toGuardResponse.hourly.data.splice(0, 12);
                    this.offlineData.setForecastByHourly(toGuardResponse);
                    return response;
                }))
                .pipe(catchError(error => {
                    console.error(error);
                    return this.offlineData.getForecastByHourly();
                }));
        } else {
            return this.offlineData.getForecastByHourly();
        }
    }

    getWeatherNow(lat: number, lon: number): Observable<OpenwthBaseModel> {
        if (this.isNetworkConnected()) {
            return this.openWeatherCloudProvider.getDailyWeather(lat, lon)
                .pipe(map(response => {
                    this.offlineData.setWeatherNow(response);
                    return response;
                }))
                .pipe(catchError(error => {
                    console.error(error);
                    return this.offlineData.getWeatherNow();
                }));
        } else {
            return this.offlineData.getWeatherNow();
        }
    }

    get5DaysForecast(lat: number, lon: number): Observable<OpenwthDailyForecastModel> {
        if (this.isNetworkConnected()) {
            return this.openWeatherCloudProvider.get5DaysForecast(lat, lon)
                .pipe(map(response => {
                    this.offlineData.set5DaysForecast(response);
                    return response;
                }))
                .pipe(catchError(error => {
                    console.error(error);
                    return this.offlineData.get5DaysForecast();
                }));
        } else {
            return this.offlineData.get5DaysForecast();
        }
    }

    private init(): void {
        Plugins.Network.addListener('networkStatusChange',
            status => {
                this.networkStatus = status;
                EventBusService.emmit('networkStatusChange', status);
            });
    }

    private isNetworkConnected() {
        return (this.networkStatus) ? this.networkStatus.connected : true;
    }
}
