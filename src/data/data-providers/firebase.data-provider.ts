import {Injectable} from '@angular/core';
import {FirebaseLocalProvider} from '../local-providers/firebase.local-provider';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class FirebaseDataProvider {
    constructor(private firebaseLocalProvider: FirebaseLocalProvider) {
    }

    upgradeFCMToken(token: string): void {
        this.firebaseLocalProvider.saveLocalToken(token);
    }

    getFCMToken(): Observable<string> {
        return this.firebaseLocalProvider.getLocalToken();
    }
}
