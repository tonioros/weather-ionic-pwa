import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {environment} from '../../environments/environment';

class EventBusModel {
    eventName: string;
    emit: any;
}

@Injectable({providedIn: 'root'})
export class EventBusService {
    private static suscribers: EventBusModel[];
    private static subject: Subject<EventBusModel>;

    public static suscribe(event: string, callback: (result: any) => void) {
        if (!EventBusService.suscribers) {
            this.init();
        }
        const suscriber = new EventBusModel();
        suscriber.eventName = event;
        suscriber.emit = callback;
        EventBusService.suscribers.push(suscriber);
        if (!environment.production) {
            console.log(`EventBut.suscribe('${event}')`, suscriber);
        }
    }

    public static emmit(event: string, result: any) {
        if (!EventBusService.subject || !EventBusService.suscribers) {
            this.init();
        }
        const suscriber = new EventBusModel();
        suscriber.eventName = event;
        suscriber.emit = result;
        if (!environment.production) {
            console.log(`EventBut.emit('${event}')`, suscriber);
        }
        EventBusService.subject.next(suscriber);
    }

    private static init() {
        EventBusService.subject = new Subject<EventBusModel>();
        EventBusService.suscribers = [];
        EventBusService.suscribeEvents();
    }

    private static suscribeEvents() {
        EventBusService.subject.asObservable().subscribe(eventResult =>
            EventBusService.suscribers.forEach(suscriber => {
                if (suscriber.eventName === eventResult.eventName) {
                    suscriber.emit(eventResult.emit);
                }
            }));
    }
}
