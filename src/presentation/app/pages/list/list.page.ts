import {Component, OnInit} from '@angular/core';
import {Get5daysForecastUseCaseService} from '../../../../domain/interactors/weather/get-5days-forecast-use-case.service';
import {BasePage} from '../base.page';
import {OpenwthBaseModel} from '../../../../data/model/open-weather/response/openwth-base.model';

@Component({
    selector: 'app-list',
    templateUrl: 'list.page.html',
    styleUrls: ['list.page.scss'],
    providers: [Get5daysForecastUseCaseService]
})
export class ListPage extends BasePage implements OnInit {
    public forecast: OpenwthBaseModel[];

    constructor(private get5daysForecast: Get5daysForecastUseCaseService) {
        super();
    }

    ngOnInit() {
        this.startLocationService().then(() => this.loadForecastInfo());
    }

    public getClassBackground(position: number) {
        const positionStr = Number(position.toString().substring(-1));
        console.log('positionStr', positionStr);
        if (positionStr > -1 && positionStr <= 2) {
            return 'purple-bk';
        } else if (positionStr > 2 && positionStr <= 4) {
            return 'green-bk';
        } else if (positionStr > 4 && positionStr <= 6) {
            return 'yellow-bk';
        } else if (positionStr > 6 && positionStr <= 9) {
            return 'rose-bk';
        }
    }

    onRefresh($event: any) {
        this.loadForecastInfo();
        setTimeout(() => {
            $event.target.complete();
        }, 1500);
    }

    private loadForecastInfo() {
        this.get5daysForecast.execute(this.lastLocation.coords.latitude,
            this.lastLocation.coords.longitude)
            .subscribe(response => {
                this.forecast = response.list;
            }, error => {
                console.error(error);
                if (this.isNetworkConnected()) {
                    this.showErrorAlert('No hemos podido obtener la informacion. Error Code: ' + error.message);
                } else {
                    this.showOfflineMessage();
                }
            });
    }
}
