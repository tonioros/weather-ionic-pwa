import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';

import {ListPage} from './list.page';
import {LottieModule} from 'ngx-lottie';
import {playerFactory} from '../../app.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        LottieModule.forRoot({player: playerFactory}),
        RouterModule.forChild([
            {
                path: '',
                component: ListPage
            }
        ])
    ],
    declarations: [ListPage]
})
export class ListPageModule {
}
