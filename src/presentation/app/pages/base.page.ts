import {MenuController, NavController, Platform, ToastController} from '@ionic/angular';
import {injector} from '../app.module';
import {EventBusService} from '../../../data/event-bus/event-bus.service';
import {AlertService} from '../shared';
import {GeolocationPosition, Plugins} from '@capacitor/core';
import {ToastOptions} from '@ionic/core';

export abstract class BasePage {
    protected alertController: AlertService;
    protected toastController: ToastController;
    protected platform: Platform;
    protected lastLocation: GeolocationPosition;
    private networkState: any;
    private navCtr: NavController;
    private menuController: MenuController;

    constructor() {
        this.alertController = injector.get(AlertService);
        this.toastController = injector.get(ToastController);
        this.platform = injector.get(Platform);
        this.navCtr = injector.get(NavController);
        this.menuController = injector.get(MenuController);
        this.preventBackButton();
    }

    async showErrorAlert(message: string, onClose?: () => void) {
        const alert = await this.alertController.create({
            header: 'Oh oh, tenemos problemas',
            buttons: ['OK'],
            message,
        });
        alert.present().then(result => {
            if (!!onClose) {
                onClose();
            }
        });
    }

    async showToast(options: ToastOptions, onClose?: () => void) {
        const toast = this.toastController.create(options);
        await toast.then(result => {
            if (!!onClose) {
                onClose();
            }
        });
    }

    public toHumanTime(unixTimestamp: number): string {
        const date = new Date(unixTimestamp * 1000);
        const hours = date.getHours();
        const minutes = '0' + date.getMinutes();
        return hours + ':' + minutes.substr(-2) + 'hrs';
    }

    public isNetworkConnected(): boolean {
        return (this.networkState) ? this.networkState.connected : false;
    }

    public toHumanDate(dateTime: string): { date: string, time: string } {
        const date = dateTime.split(' ')[0];
        const time = dateTime.split(' ')[1];
        const dateFormaterList = date.split('-');
        const timeFormaterList = time.split(':');
        const dateFormat = `${dateFormaterList[2]}/${dateFormaterList[1]}/${dateFormaterList[0]}`;
        const timeFormat = `${timeFormaterList[0]}:${timeFormaterList[1]}`;
        return {date: dateFormat, time: timeFormat};
    }

    public checkNotificationPermmitions() {
        return Notification.permission === 'granted';
    }

    protected showNetworkStateChanges(callBack: (result: any) => void) {
        EventBusService.suscribe('networkStatusChange', result => {
            this.networkState = result;
            callBack(result);
        });
    }

    protected showOfflineMessage() {
        if (this.isNetworkConnected()) {
            this.showToast({
                message: 'Estas sin conexion 😐',
                duration: 2500,
            });
        }
    }

    protected async startLocationService() {
        return Plugins.Geolocation.getCurrentPosition()
            .then(position => this.lastLocation = position,
                error => {
                    console.error(error);
                    this.showErrorAlert('No hemos podido obtener tu ubicacion. Error Code: ' + error.message);
                });
    }

    private async preventBackButton() {
        history.pushState(null, null, location.href);
        window.onpopstate = async () => {
            const menuState = await this.menuController.isOpen('main');
            if (this.alertController.isCloseAvalible()) {
                console.log('this.alertController', this.alertController);
                history.go(1);
                this.alertController.closeAll();
            } else if (menuState) {
                console.log('this.menuController', this.menuController);
                history.go(1);
                await this.menuController.close('main');
            }
        };
    }

}
