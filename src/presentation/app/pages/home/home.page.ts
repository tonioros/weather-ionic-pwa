import {Component, HostListener, OnInit} from '@angular/core';
import {GetWeatherUsecase} from '../../../../domain/interactors/weather/get-weather.use-case';
import {GetForecastByHourUseCase} from '../../../../domain/interactors/weather/get-forecast-by-hour-use-case.service';
import {OpenwthBaseModel} from '../../../../data/model/open-weather/response/openwth-base.model';
import {HourlyWeatherModel} from '../../../../data/model/dark-sky/response/hourly-weather.model';
import {BasePage} from '../base.page';
import {EventBusService} from '../../../../data/event-bus/event-bus.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
    providers: [
        GetWeatherUsecase,
        GetForecastByHourUseCase,
    ]
})
export class HomePage extends BasePage implements OnInit {
    slideOpts = {
        initialSlide: 1,
        slidesPerView: 'auto',
    };
    public weatherFullInfo: OpenwthBaseModel;
    public hourlyForecast: HourlyWeatherModel[];
    public currentlyWeather: HourlyWeatherModel;
    private weatherWidth: number;
    private weatherHeight: number;
    private animalWidth: number;
    private animalHeight: number;
    private type: number;

    constructor(private getWeatherUsecase: GetWeatherUsecase,
                private getForeCaseHourUsecase: GetForecastByHourUseCase) {
        super();
    }

    ngOnInit(): void {
        this.startLocationService().then(() =>
            this.loadWeatherInfo(this.lastLocation.coords.latitude,
                this.lastLocation.coords.longitude));
        this.onResize();
        this.showNetworkStateChanges((result: any) => this.showOfflineMessage());
    }

    @HostListener('window:resize')
    onResize() {
        this.weatherWidth = (window.innerWidth * 0.55);
        this.weatherHeight = (window.innerHeight * 0.18);
        this.animalWidth = (window.innerWidth * 0.60);
        this.animalHeight = (window.innerHeight * 0.25);
    }

    public getURLAnimalAnimation(): string {
        if (this.type) {
            switch (this.type) {
                case 0:
                    return 'assets/lottie/dinosaurio-bailando.json';
                case 1:
                    return 'assets/lottie/alce-caminando.json';
                case 2:
                    return 'assets/lottie/perrito.json';
            }
        } else {
            this.type = Math.floor(Math.random() * 3);
            return this.getURLAnimalAnimation();
        }
    }

    public onRefresh($event): void {
        this.loadWeatherInfo(
            this.lastLocation.coords.latitude,
            this.lastLocation.coords.longitude);
        setTimeout(() => {
            $event.target.complete();
        }, 1500);
    }

    public getWeatherAnimation(weatherIcon: string) {
        switch (weatherIcon) {
            case 'clear-day':
                return 'assets/lottie/soleado.json';
            case 'clear-night':
            case 'partly-cloudy-night':
                return 'assets/lottie/noche.json';
            case 'rain':
                return 'assets/lottie/lloviendo.json';
            case 'wind':
            case 'fog':
                return 'assets/lottie/nublado-viento.json';
            case 'cloudy':
            case 'partly-cloudy-day':
                return 'assets/lottie/parcialmente-nublado.json';
        }
    }

    searchForecast() {
        this.showErrorAlert('Buenos dias ');
    }

    onClickNotification() {
        EventBusService.emmit('initialiceFCM', true);
    }

    private loadWeatherInfo(lat: number, lon: number) {
        this.getWeatherUsecase.execute(lat, lon)
            .subscribe(weatherInfo => {
                this.weatherFullInfo = weatherInfo;
            }, error => {
                console.error(error);
                if (this.isNetworkConnected()) {
                    this.showErrorAlert('No hemos podido obtener la informacion. Error Code: ' + error.message);
                } else {
                    this.showOfflineMessage();
                }
            });

        this.getForeCaseHourUsecase.execute(lat, lon)
            .subscribe(hourlyInfo => {
                this.hourlyForecast = hourlyInfo.hourly.data;
                this.currentlyWeather = hourlyInfo.currently;
            }, error => {
                console.error(error);
                if (this.isNetworkConnected()) {
                    this.showErrorAlert('No hemos podido obtener la informacion. Error Code: ' + error.message);
                } else {
                    this.showOfflineMessage();
                }
            });
    }
}
