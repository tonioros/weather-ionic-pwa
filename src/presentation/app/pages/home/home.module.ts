import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';

import {HomePage} from './home.page';
import {LottieModule} from 'ngx-lottie';
import {playerFactory} from '../../app.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        LottieModule.forRoot({player: playerFactory}),
        RouterModule.forChild([
            {
                path: '',
                component: HomePage
            }
        ])
    ],
    declarations: [HomePage]
})
export class HomePageModule {
}
