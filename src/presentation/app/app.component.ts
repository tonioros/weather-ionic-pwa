import {Component, OnInit} from '@angular/core';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {MENU_PAGES_LIST} from './pages/menu/menu-list';
import {UpgradeFcmTokenUsecase} from '../../domain/interactors/firebase/upgrade-fcm-token.usecase';
import * as firebase from 'firebase/app';
import 'firebase/messaging';
import {environment} from '../../environments/environment';
import {EventBusService} from '../../data/event-bus/event-bus.service';
import {GetFcmTokenUsecase} from '../../domain/interactors/firebase/get-fcm-token.usecase';
import {Platform, ToastController} from '@ionic/angular';
import {ToastOptions} from '@ionic/core';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
    providers: [
        UpgradeFcmTokenUsecase,
        GetFcmTokenUsecase,
    ]
})
export class AppComponent implements OnInit {
    public appPages: any[] = MENU_PAGES_LIST;

    constructor(
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private upgradeFCMToken: UpgradeFcmTokenUsecase,
        private getFCMToken: GetFcmTokenUsecase,
        private platform: Platform,
        private toastController: ToastController
    ) {
        this.initializeApp();
    }

    ngOnInit(): void {
        if (Notification.permission === 'granted') {
            setTimeout(() => this.verifyFCMConnection(), 2000);
        } else {
            EventBusService.suscribe('initialiceFCM', (result) => this.verifyFCMConnection(true));
        }
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    public verifyFCMConnection(initialStared?: boolean) {
        this.getFCMToken.execute().subscribe(token => {
            if (!token || token.trim().length === 0) {
                this.fcmServices(initialStared);
            }
        });
    }

    public fcmServices(initialStared?: boolean) {
        console.log('FCM start');
        firebase.initializeApp(environment.firebase);
        navigator.serviceWorker.ready.then((registration) => {
            Notification.requestPermission().then((result) => {
                if (result === 'granted') {
                    try {
                        this.initFCMServices(registration);
                        if (initialStared) {
                            registration.showNotification('Hola!', {
                                body: 'Asi se presentaran las notificaciones',
                                icon: '/assets/icon/favicon.png',
                                vibrate: [200, 100],
                                tag: 'vibration-sample'
                            });
                        }
                    } catch (e) {
                        console.error('Error al iniciar los servicios de FCM');
                        console.error(e);
                    }
                } else {
                    this.showToast({
                        message: 'Si deseas recibir notificaciones, debes de permitir mostrar notificaciones',
                        duration: 3000,
                        buttons: [
                            {
                                text: 'Reintentar',
                                handler: this.fcmServices
                            }
                        ]
                    });
                }
            });
        });
    }

    public async initFCMServices(registration: ServiceWorkerRegistration) {
        const messaging = firebase.messaging();
        messaging.useServiceWorker(registration);
        messaging.usePublicVapidKey(environment.fcm_pVapiKey);
        messaging.onMessage((payload) => {
            console.log('onMessage payload', payload);
        });
        messaging.onTokenRefresh(() => {
            messaging.getToken().then(token => {
                console.log('refresh token', token);
                if (this.upgradeFCMToken) {
                    this.upgradeFCMToken.execute(token);
                }
            }).catch(r => console.log('refresh token catch', r));
        });
        messaging.getToken().then(token => {
            console.log('refresh token', token);
            if (this.upgradeFCMToken) {
                this.upgradeFCMToken.execute(token);
            }
        }).catch(r => console.log('refresh token catch', r));

        registration.addEventListener('push', (event: any) => {
            console.log('Received a push message', event);
            event.waitUntil(registration.showNotification('Hola!', {
                body: 'Asi se presentaran las notificaciones',
                icon: '/assets/icon/favicon.png',
                vibrate: [200, 100],
                tag: 'vibration-sample'
            }));
        });
    }

    async showToast(options: ToastOptions, onClose?: () => void) {
        const toast = this.toastController.create(options);
        await toast.then(result => {
            if (!!onClose) {
                onClose();
            }
        });
    }

}
