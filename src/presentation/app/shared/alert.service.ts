import {Injectable} from '@angular/core';
import {AlertController} from '@ionic/angular';
import {AlertOptions} from '@ionic/core';
import {randomString} from './functions';

@Injectable({
    providedIn: 'root'
})
export class AlertService {
    alertsShowing: any;

    constructor(private alertComponent: AlertController) {
        this.alertsShowing = [];
    }

    public async create(options: AlertOptions) {
        const alert = await this.alertComponent.create(options);
        const hash = randomString(4);
        this.alertsShowing[hash] = alert;
        alert.onWillDismiss()
            .then(() => {
                this.alertsShowing[hash] = undefined;
            });
        return alert;
    }

    public isCloseAvalible() {
        return Object.keys(this.alertsShowing).length;
    }

    public closeAll() {
        if (Object.keys(this.alertsShowing).length > 0) {
            for (const key of Object.keys(this.alertsShowing)) {
                console.log('this.alertsShowing[key]', this.alertsShowing[key]);
                if (this.alertsShowing[key]) {
                    this.alertsShowing[key].dismiss();
                }
            }
        }
        this.alertsShowing = undefined;
        this.alertsShowing = {};
    }
}
