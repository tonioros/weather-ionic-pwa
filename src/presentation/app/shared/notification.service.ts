import {Injectable} from '@angular/core';
import {firebase} from '@firebase/app';
import '@firebase/messaging';
import {environment} from '../../../environments/environment';

@Injectable({providedIn: 'root'})
export class NotificationService {
    private storageService: any;

    async init(storageService?: any) {
        // this.requestPermission();
        this.storageService = storageService;

    }
}
