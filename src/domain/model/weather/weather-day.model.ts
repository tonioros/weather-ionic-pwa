export class WeatherDayModel {
    main: string;
    description: string;
    temp: number;
    pressure: number;
    humidity: number;
    tempMin: number;
    tempMax: number;
    windSpeed: number;
    windDeg: number;
    cloudsPercent: number;
    sunrise: number;
    sunset: number;
    cityName: string;
}
