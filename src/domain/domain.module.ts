import {NgModule} from '@angular/core';
import {DataModule} from '../data/data.module';

@NgModule({
    imports: [
        DataModule,
    ]
})
export class DomainModule {
}
