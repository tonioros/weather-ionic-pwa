import {Injectable} from '@angular/core';
import {FirebaseDataProvider} from '../../../data/data-providers/firebase.data-provider';

@Injectable()
export class UpgradeFcmTokenUsecase {
    constructor(private firebaseDataProvider: FirebaseDataProvider) {
    }

    execute(token: string): void {
        this.firebaseDataProvider.upgradeFCMToken(token);
    }
}
