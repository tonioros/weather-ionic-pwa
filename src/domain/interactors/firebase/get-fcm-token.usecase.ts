import {Injectable} from '@angular/core';
import {FirebaseDataProvider} from '../../../data/data-providers/firebase.data-provider';
import {Observable} from 'rxjs';

@Injectable()
export class GetFcmTokenUsecase {
    constructor(private firebaseDataProvider: FirebaseDataProvider) {
    }

    execute(): Observable<string> {
        return this.firebaseDataProvider.getFCMToken();
    }
}
