import {Injectable} from '@angular/core';
import {WeatherDataProvider} from '../../../data/data-providers/weather.data-provider';
import {Observable} from 'rxjs';
import {WeatherBaseModel} from '../../../data/model/dark-sky/response/weather-base.model';

@Injectable()
export class GetForecastByHourUseCase {
    constructor(private weatherDataProvider: WeatherDataProvider) {
    }

    /**
     * Request details of Weather now based of an specific Latitude and longitude
     * @param lat - latitude to consult
     * @param lon - longitude to consult
     */
    execute(lat: number, lon: number): Observable<WeatherBaseModel> {
        return this.weatherDataProvider.getForecastByHourly(lat, lon);
    }
}
