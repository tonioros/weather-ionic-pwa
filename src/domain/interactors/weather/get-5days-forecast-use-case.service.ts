import {Injectable} from '@angular/core';
import {WeatherDataProvider} from '../../../data/data-providers/weather.data-provider';
import {Observable} from 'rxjs';
import {OpenwthDailyForecastModel} from '../../../data/model/open-weather/response/openwth-daily-forecast.model';

@Injectable()
export class Get5daysForecastUseCaseService {
    constructor(private weatherDataProvider: WeatherDataProvider) {
    }

    /**
     * Request details of Weather now based of an specific Latitude and longitude
     * @param lat - latitude to consult
     * @param lon - longitude to consult
     */
    execute(lat: number, lon: number): Observable<OpenwthDailyForecastModel> {
        return this.weatherDataProvider.get5DaysForecast(lat, lon);
    }
}
