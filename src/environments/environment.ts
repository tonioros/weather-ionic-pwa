// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    dark_sky_apitoken: '369ed35aba1df79b8e0451ee87335c9d',
    open_weather_apitoken: '103771d16b5d30c9f24322613b4bba27',
    fcm_pVapiKey: 'BBs7muK0qMpEixx2dBbSF9HFUdjlfQZwB2Q92Xv7E8WebuNppZ782fjXC5MqWE6AuUyyT60LiMlD9_RnIid1u4E',
    firebase: {
        apiKey: 'AIzaSyC11_UvJAFL916EQb6miUXmAZYBkJkryIU',
        authDomain: 'weather-ionic.firebaseapp.com',
        databaseURL: 'https://weather-ionic.firebaseio.com',
        projectId: 'weather-ionic',
        storageBucket: 'weather-ionic.appspot.com',
        messagingSenderId: '251194730149',
        appId: '1:251194730149:web:193bb9e67d25a6a3f4cf4c',
        measurementId: 'G-EH61H6XMXH'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
